FROM d4science/r-full:latest

LABEL org.d4science.image.licenses="EUPL-1.2" \
      org.d4science.image.source="https://code-repo.d4science.org/gCubeSystem/rstudio-base" \
      org.d4science.image.vendor="D4Science <https://www.d4science.org>" \
      org.d4science.image.authors="Andrea Dell'Amico <andrea.dellamico@isti.cnr.it>, Roberto Cirillo <roberto.cirillo@isti.cnr.it>"

ENV DEBIAN_FRONTEND=noninteractive

ENV S6_VERSION=v2.1.0.2
ENV RSTUDIO_VERSION=2023.03.0+386
ENV DEFAULT_USER=rstudio
ENV PANDOC_VERSION=default
ENV QUARTO_VERSION=default

ARG NB_USER="jovyan"
ARG NB_UID="1000"
ARG NB_GID="100"
ARG HOME="/home/${NB_USER}"
ENV S6_VERSION=v2.1.0.2
ENV RSTUDIO_VERSION=2022.02.1+461
ENV DEFAULT_USER="jovyan"
ENV USERID="1000"
ENV GROUPID="100"
ENV PATH=/usr/lib/rstudio-server/bin:$PATH
ENV R_HOME=/usr/local/lib/R
# Configure environment
ENV SHELL=/bin/bash \
    NB_USER="${NB_USER}" \
    NB_UID=${NB_UID} \
    NB_GID=${NB_GID} \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8
ENV HOME="/home/${NB_USER}"

RUN apt-get update --yes
RUN /rocker_scripts/install_rstudio.sh
RUN /rocker_scripts/install_pandoc.sh
RUN /rocker_scripts/install_quarto.sh

# This part comes from https://github.com/openanalytics/shinyproxy-rstudio-ide-demo/blob/master/Dockerfile
RUN echo "www-frame-origin=same" >> /etc/rstudio/disable_auth_rserver.conf
RUN echo "www-verify-user-agent=0" >> /etc/rstudio/disable_auth_rserver.conf

RUN mv -f /etc/rstudio/disable_auth_rserver.conf /etc/rstudio/rserver.conf
RUN echo "USER=$NB_USER" >> /etc/environment
COPY rsession.conf /etc/rstudio/rsession.conf
RUN chmod 0644 /etc/rstudio/rsession.conf
RUN curl -o "${R_HOME}/etc/Rprofile.site" "https://code-repo.d4science.org/gCubeSystem/rstudio-rprofile/raw/branch/master/jupyter-Rprofile.site"

# Copy a script that we will use to correct permissions after running certain commands
COPY fix-permissions /usr/local/bin/fix-permissions
RUN chmod a+rx /usr/local/bin/fix-permissions

# Create NB_USER with name jovyan user with UID=1000 and in the 'users' group
# and make sure these dirs are writable by the `users` group.
RUN echo "auth requisite pam_deny.so" >> /etc/pam.d/su && \
    sed -i.bak -e 's/^%admin/#%admin/' /etc/sudoers && \
    sed -i.bak -e 's/^%sudo/#%sudo/' /etc/sudoers && \
    userdel -r "${NB_USER}" && \
    useradd -l -m -s /bin/bash -N -u "${NB_UID}" "${NB_USER}" && \
    chmod g+w /etc/passwd && \
    fix-permissions "${HOME}"

# Install the jupiterhub proxy
RUN pip3 install \
    jsonschema'[format,format-nongpl]' \
    jupyterhub \
    jupyterlab \
    jupyter-rsession-proxy \
    jupyter-server-proxy \
    notebook

RUN apt-get clean && rm -rf /var/lib/apt/lists/* && \
    echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

# Enable prompt color in the skeleton .bashrc before creating the default NB_USER
# hadolint ignore=SC2016
RUN sed -i 's/^#force_color_prompt=yes/force_color_prompt=yes/' /etc/skel/.bashrc

WORKDIR ${HOME}
